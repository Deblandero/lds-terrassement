<html>
    <head>
        <title>@yield('title')</title>

        <link rel="shortcut icon" href="./img/excavator.png" type="image/x-icon">

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

        <!-- Font Awesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
            crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="/css/app.css">
    

    </head>
    <body>

        {{-- NAVIGATION --}}
        <nav class="navbar navbar-expand-lg navbar-dark bg-navbar fixed-top">
            
            <img src="/img/excavator.png" width="30" height="30" class="d-inline-block align-top mr-2" alt="">
            <a class="navbar-brand" href="{{route('acceuil')}}">LDS Terrassement</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse ml-3" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ Route::currentRouteNamed('acceuil') ? 'active' : "" }}">
                    <a class="nav-link" href="{{route('acceuil')}}">Acceuil<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item {{ Route::currentRouteNamed('presentation') ? 'active' : "" }}">
                    <a class="nav-link" href="{{route('presentation')}}">Présentation</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Services
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item">Terrassement & Nivellement</a>
                        <a class="dropdown-item">Aménagement extérieurs</a>
                        <a class="dropdown-item">Démolition</a>
                        <a class="dropdown-item">Epuration & citernes</a>
                        </div>
                    </li>
                    <li class="nav-item {{ Route::currentRouteNamed('realisations') ? 'active' : "" }}">
                    <a class="nav-link" href="{{route('realisations')}}">Réalisation</a>
                    </li>
                </ul>
            </div>
        </nav>
        {{-- END NAVIGATION --}}

        {{-- CONTENT --}}
        <div class="container">
            @yield('content')
        </div>
        {{-- END CONTENT --}}

        {{-- FOOTER --}}
        <div class="row fixed-bottom mx-auto text-center bg-light">
            <div class="col-md-6 offset-md-3 sticky-bottom my-3">
                <span class="d-inline p-2">LDS Terrassement</span>
                <span class="d-inline p-2">Rue des Fonds, 50 - 5170 PROFONDEVILLE</span>
                <span class="d-inline p-2">Tél. 081 41 14 95 - GSM. 0495 204 114</span>
            </div>
        </div>
        {{-- END FOOTER --}}

        <!-- JS -->
        <script type="text/javascript" src="/js/app.js"></script>       
    </body>
</html>