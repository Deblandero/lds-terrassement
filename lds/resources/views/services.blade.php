{{-- utilise le layout app, dans /resources/views/layouts/app.blade.php --}}
@extends('layouts.app')

{{-- Remplis la directive-variable title dans le template app 
    dans le themplate on la trouve via la directive @yield
    --}}
@section('title', 'LDS Terrassement')

@section('content')
<div class="terr" id="1">
    <h1>Terrassement et nivellement</h1>
</div>
<div class="terr" id="2">
    <h1>Aménagement ext</h1>
</div>
<div class="terr" id="3">
    <h1>Démolition</h1>
</div>
<div class="terr" id="4">
    <h1>Epuration et citerne</h1>
</div>
@endsection