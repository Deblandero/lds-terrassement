{{-- utilise le layout app, dans /resources/views/layouts/app.blade.php --}}
@extends('layouts.app')

{{-- Remplis la directive-variable title dans le template app 
    dans le themplate on la trouve via la directive @yield
    --}}
@section('title', 'LDS Terrassement')

@section('content')
    <div class="container">
        <div class="row mt-7 mx-auto">
            <div class="col-md-4 offset-md-2">
                <div class="card mt-2 mb-2">
                    <img class="card-img-top" src=".../100px180/" alt="Card image cap" style="height:200px; widht:auto;">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mt-2 mb-2">
                    <img class="card-img-top" src=".../100px180/" alt="Card image cap" style="height:200px; widht:auto;">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-7 mx-auto">
            <div class="col-md-4 offset-md-2">
                <div class="card mt-2 mb-2">
                    <img class="card-img-top" src=".../100px180/" alt="Card image cap" style="height:200px; widht:auto;">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mt-2 mb-2">
                    <img class="card-img-top" src=".../100px180/" alt="Card image cap" style="height:200px; widht:auto;">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-7 mx-auto mb-7">
            <div class="col-md-4 offset-md-2">
                <div class="card mt-2 mb-2">
                    <img class="card-img-top" src=".../100px180/" alt="Card image cap" style="height:200px; widht:auto;">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mt-2 mb-2">
                    <img class="card-img-top" src=".../100px180/" alt="Card image cap" style="height:200px; widht:auto;">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection