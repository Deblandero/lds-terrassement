{{-- utilise le layout app, dans /resources/views/layouts/app.blade.php --}}
@extends('layouts.app')

{{-- Remplis la directive-variable title dans le template app 
    dans le themplate on la trouve via la directive @yield
    --}}
@section('title', 'LDS Terrassement')


@section('content')
<div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-8 mx-auto">
                <div class="jumbotron text-center">
                    <h1 class="display-4">LDS TERRASSEMENT</h1>
                    <p class="lead">une entreprise familiale qui est située à Lustin en Belgique depuis 1996.</p>
                </div>
            </div>
        </div>
    </div>
@endsection