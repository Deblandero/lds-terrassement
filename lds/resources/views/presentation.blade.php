{{-- utilise le layout app, dans /resources/views/layouts/app.blade.php --}}
@extends('layouts.app')

{{-- Remplis la directive-variable title dans le template app 
    dans le themplate on la trouve via la directive @yield
    --}}
@section('title', 'LDS Terrassement')

@section('content')
<div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-md-4 mx-auto">
                <h2>Qui sommes nous ?</h2>
                <p>L'activité principale de LDS Terrassement est de réalisée vos souhaits dans les règles de l'art et avec une très grande précision.</p>
                <p>une petite cannette n'est jamais de refu...</p>
            </div>
            <div class="col-md-4 mx-auto">
                <h2>Que faisons nous ?</h2>
                <p>LDS Terrassementn, effectue également les aménagements de maisons, ce qui comprend la réalisation de parking avec empierrement et finition souhaité mais aussi le nivellement des terres.</p>
                <p>Nous Proposons également une large gamme de piscine en coque ou piscines en béton selon les désires et ce afin d'embellir les espace privé.</p>
            </div>
        </div>
    </div>
@endsection